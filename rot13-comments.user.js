// ==UserScript==
// @name           ROT13
// @version        1.5
// @namespace      stackexchange
// @description    Automate rot13 spoiler text in comments. To insert a rot13 spoiler into a comment, include: "rot13(your message)" and click the "ROT13" link that appears to the right. To read a rot13 spolier in a comment, simply hover/click.
// @grant          none
// @include        http*://*.stackexchange.com/*
// @include        http*://*stackoverflow.com/*
// @include        http*://*serverfault.com/*
// @include        http*://*superuser.com/*
// @include        http*://*mathoverflow.net/*
// @include        http*://*stackapps.com/*
// @include        http*://*askubuntu.com/*
// @author         Alconja
// ==/UserScript==

function with_jquery(f) {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.textContent = "(" + f.toString() + ")(jQuery)";
  document.body.appendChild(script);
};

with_jquery(function ($) {

  function rot13(s) {
    //https://stackoverflow.com/a/617685/68727
    return s.replace(/[a-zA-Z]/g,function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);});
  }
  
  function rot13Node(n) {
    for (var i = 0; i < n.childNodes.length; i++) {
      if (n.childNodes[i].nodeName == "#text") {
        n.childNodes[i].nodeValue = rot13(n.childNodes[i].nodeValue);
      } else {
        rot13Node(n.childNodes[i]);
      }
    }  
  }
  
  function rot13Html(j) {
    var newJ = j.clone();
    rot13Node(newJ[0]);
    return newJ.html();
  }
  
  function showTip(elem, text) {
    $(elem).find(".tip").html(text).show();
  }
  
  function hideTip(elem) {
    var tip = $(elem).find(".tip");
    if (tip.css("position") == "absolute") {      //floating...
      $(elem).find(".tip").hide();
    }   //else ignore (click to hide)
  }
    
  function toggleTip(elem, text) {
    var tip = $(elem).find(".tip");
    if (tip.css("position") == "absolute") {  //floating...
      tip.css("position", "relative");
      showTip(elem, text);
    } else {    //fixed
      tip.css("position", "absolute");
      hideTip(elem);
    }
  }

  $(document).ready(function () {

    //there...

    //#mainbar .js-comment-text-input ==> normal SE sites
    //#input-area #input ==> chat
    $("#mainbar, #input-area").on("keyup", ".js-comment-text-input, #input", (evt) => {
      var field = $(evt.currentTarget);
      if (!field.data("hasRot")) {
        var hasRot = field.val().match(/rot13\(([^\)]*)\)/gi);
        if (hasRot) {
          var rotLink = $("<a>ROT13</a>")
            .addClass("comment-help-link")
          	.css('cursor', 'pointer') //for chat
            .click(() => {
              var val = field.val();
              var out = val;
              var reg = /rot13((?!rot13).)*/gi;
              var found;
              while(found = reg.exec(val)) {
                  var charIndex = "rot13".length;
                  var nestLevel = +(found[0][charIndex] == '(');
                  charIndex++;
                  var rotText = "";
                  while(nestLevel > 0 && charIndex < found[0].length) {
                      if(found[0][charIndex] == '(') {nestLevel++;}
                      if(found[0][charIndex] == ')') {nestLevel--;}
                      if(nestLevel > 0)
                      {rotText += found[0][charIndex];}
                      charIndex++;
                  }
                  if (rotText.length > 0) {
                    var text = rot13(rotText);
                    out = out.replace(rotText, text);
                  }
              }
              field.val(out);
            });
          //.js-comment-form-layout button[type=submit] ==> normal SE sites
          //#input-table #chat-buttons button:last ==> chat
          field.closest("#input-table, .js-comment-form-layout").find("#chat-buttons button:last, button[type=submit]").after(rotLink);
          field.data("hasRot", true);
        }
      }

    });

    //...and back again

    //#mainbar .comment-copy ==> normal SE sites
    //#chat .message ==> chat
    $("#mainbar, #chat, #transcript").on("mouseenter", ".comment-copy, .content", (evt) => {
      var comment = $(evt.currentTarget);
      if (!comment.data("rotProcessed")) {
        comment.data("rotProcessed", true);
        var html = comment.html();
        var fullRot = rot13Html(comment);
        var tip = $("<div class='tip' style='display:none;position:absolute;border:solid #c8ccd0 1px; margin:7px 0 2px -1px;background:#eff0f1;z-index:9999'/>");
        tip.css("width", comment.css("width")).css("width", "+=2");
        comment.closest(".comment-body, .message").append(tip);
        var reg = /rot13((?!rot13).)*/gi;
        var found;
        var out = "";
        var lastIdx = 0;
        var lastLen = 0;
        while(found = reg.exec(html)) {
          var charIndex = "rot13".length;
          var nestLevel = +(found[0][charIndex] == '(');
          charIndex++;
          var rotText = "";
          var text = "";
          while(nestLevel > 0 && charIndex < found[0].length) {
            if(found[0][charIndex] == '(') {nestLevel++;}
            if(found[0][charIndex] == ')') {nestLevel--;}
            if(nestLevel > 0) {
                text += found[0][charIndex];
                rotText += fullRot[found.index + charIndex];
            }
            charIndex++;
          }
          var link;
          var highlighted;
          if (rotText.length > 0) {
            //official format...
            highlighted = `${html.slice(0, found.index)}<span style="background-color:#ffed99">${rotText}</span>${html.slice(found.index + charIndex)}`;
            link = `${found[0].substr(0, "rot13".length)}(<span style="background:#ffed99;cursor:help" data-highlighted="${highlighted.replace(/"/g, "&quot;")}" >${text}</span>)`;
          } else {
            //generic fallback...
            charIndex--;    //back it up for the splicing below...
            text = found[0].substr(0, "rot13".length);
            highlighted = `${fullRot.slice(0, found.index)}<span style="background-color:#c8ccd0">${text}</span>${fullRot.slice(found.index + charIndex)}`;
            link = `<span style="background:#c8ccd0;cursor:help" data-highlighted="${highlighted.replace(/"/g, "&quot;")}" >${text}</span>`;
          }
          
          out += html.slice(lastIdx, found.index);
          out += link;
          lastIdx = found.index + charIndex;
        }
        out += html.slice(lastIdx, html.length);
        //if we actually changed anything switch the content (storing both before and after html to save reprocessing)
        if (out.length > html.length) {
          comment.data("preRot", html);
  	      comment.data("postRot", out);
          comment.on("mouseenter", "span[data-highlighted]", (evt) => {
              showTip($(evt.currentTarget).closest(".comment-copy, .content").closest(".comment-body, .message"), $(evt.currentTarget).data("highlighted"));
          })
          .on("mouseleave", "span[data-highlighted]", (evt) => {
              hideTip($(evt.currentTarget).closest(".comment-copy, .content").closest(".comment-body, .message"));
          })
          .on("click", "span[data-highlighted]", (evt) => {
              toggleTip($(evt.currentTarget).closest(".comment-copy, .content").closest(".comment-body, .message"), $(evt.currentTarget).data("highlighted"));
              return false;
          });
        }
      }

      if (comment.data("postRot") && !comment.data("hasRot")) {
        comment.data("hasRot", true);
        comment.html(comment.data("postRot"));
      }
    })
    .on("mouseleave", ".comment-copy, .content", (evt) => {
      var comment = $(evt.currentTarget);
      if (comment.data("hasRot")) {
        comment.data("hasRot", false);
        comment.html(comment.data("preRot"));
      }
    });
  });
});